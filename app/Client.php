<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends XModel
{
    //
    protected $table = 'clients';
    protected $fillable = ['name', 'client_name','paid_amount'];
    
}
