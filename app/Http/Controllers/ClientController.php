<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function showClients()
    {
        $client = Client::get();
        return json_encode($client);
    }
    
    
    public function showClient($id)
    {
        $client = Client::find($id);
        return json_encode($client);
    }


    public function createClient(Request $request)
    {
        try {
            $client = new Client([
                'client_name' => $request->get('client_name'),
                'client_address' => $request->get('client_address'),
                'due_amount' => $request->get('due_amount'),
                'paid_amount' => $request->get('paid_amount')
            ]);
            $client->save();
            return json_encode(['status' => 'success', 'message' => 'Client Saved Successfully']);
        } catch (\Exception $ex) {
            return json_encode(['status' => 'faild', 'message' => $ex->getMessage()]);
        }
    }
    
    public function deleteClient($id)
    {
            $client = Client::find($id);
            
            $client->delete();
            return json_encode(['status' => 'success', 'message' => 'Client Deleted Successfully']);
        
    }
}
