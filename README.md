# Laravel Test
This is a dummy task that we use as a test during our Laravel and backend interviews.


### Task:
- Run this project on any CentOS/Ubuntu VPS server (You may use one from AWS, DigitialOcean)
- This project has some  intentional errors that you need to fix.

### Verify Setup:

- The root url should display default laravel page
- GET /clients : should return a json array of the `Client` model
- POST /clients : should be able to create new clients in the database with following attributes
    - client_name
    - client_address
    - due_amount
    - paid_amount



### Example
Refer to http://task-laravel.betacloud.tech/, this url has the correct setup of this repo and yours should work exactly like this.

All the best!
--